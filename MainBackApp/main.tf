provider "kubernetes" {
  config_path    = "/usr/share/kubeconfig/config"
  config_context = "kubernetes-admin@kubernetes"
}

####################################
#    Deploy Main App             ###
####################################

resource "kubernetes_deployment" "main" {
  metadata {
    name = "main"
    #namespace= "nginx-ingress"
    labels = {
      app = "main"
    }
  }

  spec {
    replicas = 1

  selector {
      match_labels = {
        app = "main"
      }
    }

    template {
      metadata {
        labels = {
          app = "main"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/arcadia-application/main-app/mainapp:latest"
          name  = "main"
          image_pull_policy = "IfNotPresent"

          port {
            container_port = "80"
           }

 }
        }
      }
    }
}

resource "kubernetes_service" "main" {
  metadata {
    name = "main"
    #namespace= "nginx-ingress"
    labels = {
      app = "main"
      service = "main"
    }
  }
  spec {
    selector = {
      app = "main"
    }
    port {
      port        = 80
      target_port = 80
      node_port = 30511
    }

    type = "NodePort"
  }
}

####################################
#    Deploy BackEnd              ###
####################################

resource "kubernetes_deployment" "backend" {
  metadata {
    name = "backend"
    #namespace= "nginx-ingress"
    labels = {
      app = "backend"
    }
  }

  spec {
    replicas = 1

  selector {
      match_labels = {
        app = "backend"
      }
    }

    template {
      metadata {
        labels = {
          app = "backend"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/arcadia-application/back-end/backend:latest"
          name  = "backend"
          image_pull_policy = "IfNotPresent"

          port {
            container_port = "80"
           }

 }
        }
      }
    }
}

resource "kubernetes_service" "backend" {
  metadata {
    name = "backend"
   #namespace= "nginx-ingress"
    labels = {
      app = "backend"
      service = "backend"
    }
  }
  spec {
    selector = {
      app = "backend"
    }
    port {
      port        = 80
      target_port = 80
      node_port = 31584
    }

    type = "NodePort"
  }
}

####################################
#    Deploy Ingress            ###
####################################
resource "kubernetes_ingress_v1" "arcadia_ingress" {
  metadata {
    name      = "arcadia-ingress"
    #namespace = "nginx-ingress"

    annotations = {
      "appprotect.f5.com/app-protect-enable" = "True"

      "appprotect.f5.com/app-protect-policy" = "default/dataguard-alarm"

      "appprotect.f5.com/app-protect-security-log" = "default/logconf"

      "appprotect.f5.com/app-protect-security-log-destination" = "syslog:server=127.0.0.1:514"

      "appprotect.f5.com/app-protect-security-log-enable" = "True"

      #"kubernetes.io/ingress.class" = "nginx"
    }
  }

  spec {
    ingress_class_name = "nginx"
    tls {
      hosts       = ["arcadia.westcon.com"]
      secret_name = "arcadia-secret"
    }

    rule {
      host = "arcadia.westcon.com"

      http {
        path {
          path = "/"

          backend {
            service {
              name = "main"
              port {
                number = "80"
              }
            }
           /* service_name = "main"
            service_port = "80"*/
          }
        }

        path {
          path = "/files"

          backend {
            service {
              name = "backend"
              port {
                number = "80"
              }
            }
            /*service_name = "backend"
            service_port = "80"*/
          }
        }

        path {
          path = "/api"

          backend {
            service {
              name = "app2"
              port {
                number = "80"
              }
            }
            /*service_name = "app2"
            service_port = "80"*/
          }
        }

        path {
          path = "/app3"

          backend {
            service {
              name = "app3"
              port {
                number = "80"
              }
            }
            /*service_name = "app3"
            service_port = "80"*/
          }
        }
      }
    }
  }
}
